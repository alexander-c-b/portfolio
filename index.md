---
author: Alexander C. Bodoh
title: Alexander C. Bodoh's Project Portfolio
---

Welcome to my project portfolio! This webpage lists various projects I've worked
on for research and personal endeavors. Click the link associated with any of
the titles to view more information about the project on its repository, or on
IEEE Xplore or MDPI for my research publications.

## Research Papers

### ["A Scalable Formal Framework for the Verification and Vulnerability Analysis of Redundancy-Based Error-Resilient Null Convention Logic Asynchronous Circuits"](https://doi.org/10.3390/jlpea14010005) {#scalable-formal-framework}

![](./images/scalable-formal-framework.png)

> Mazumder, D.; Datta, M.; Bodoh, A.C.; Sakib, A.A. A Scalable Formal Framework
> for the Verification and Vulnerability Analysis of Redundancy-Based
> Error-Resilient Null Convention Logic Asynchronous Circuits. J. Low Power
> Electron. Appl. 2024, 14, 5. <https://doi.org/10.3390/jlpea14010005>

Our work describes a novel methodology to formally prove the
radiation-resilience of error-tolerant null convention logic circuits. I served
as the primary software developer on the project, creating tools to apply our
formal methods to example circuits for analysis in the paper.

This paper is an open-access journal article, meaning that anyone can read the
full text without a journal subscription.

### ["Combining Relaxation With NCL_X for Enhanced Optimization of Asynchronous Null Convention Logic Circuits"](https://doi.org/10.1109/ACCESS.2023.3318132) {#relaxation-ncl-x}

![](./images/relaxation_ncl_x_thumbnail.png)

> D. Khodosevych, A. C. Bodoh, A. A. Sakib and S. C. Smith, "Combining
> Relaxation With NCL_X for Enhanced Optimization of Asynchronous Null
> Convention Logic Circuits," in *IEEE Access*, vol. 11, pp. 104688-104699,
> 2023, doi: 10.1109/ACCESS.2023.3318132.

This publication describes our novel method for improving the speed and size of
future low-power hardware, to overcome the slowing rate of improvement in modern
hardware design. We were able to achieve an average speed increase of 6.8% to
12.5% over existing optimization methods for Null Convention Logic, measured by
the total number of "layers" or levels of gates between each example device's
inputs and outputs.

This paper is an open-access journal article, meaning that anyone can read the
full text without a journal subscription.

--------------------------------------------------------------------------------

### ["Error Resilient Sleep Convention Logic Asynchronous Circuit Design"](https://doi.org/10.1109/NEWCAS57931.2023.10198041)

![](./images/error-resilient-thumbnail.png)

> M. Datta, A. Bodoh and A. A. Sakib, "Error Resilient Sleep Convention Logic
> Asynchronous Circuit Design," *2023 21st IEEE Interregional NEWCAS Conference
> (NEWCAS)*, Edinburgh, United Kingdom, 2023, pp. 1-5, doi:
> 10.1109/NEWCAS57931.2023.10198041.

We developed a design strategy for creating error- and radiation-resilient
circuits with 65% lower power usage when idle and 18% lower area usage compared
to an existing low-power, radiation-resilient design strategy (described in
[this paper](https://doi.org/10.3390/jlpea5040216) by Zhou et al.).

--------------------------------------------------------------------------------

### ["Comparative Analysis of CNTFET and CMOS based NCL Asynchronous Circuits: A Study of Scaling Trends"](https://doi.org/10.1109/UEMCON54665.2022.9965723)

![](./images/cntfet.png)

> A. C. Bodoh and A. A. Sakib, "Comparative Analysis of CNTFET and CMOS based
> NCL Asynchronous Circuits: A Study of Scaling Trends," *2022 IEEE 13th Annual
> Ubiquitous Computing, Electronics & Mobile Communication Conference (UEMCON)*,
> New York, NY, NY, USA, 2022, pp. 0407-0412, doi:
> 10.1109/UEMCON54665.2022.9965723.

This publication, co-authored with my research supervisor and mentor Dr. Ashiq
Sakib, simulated NULL-Convention Logic asynchronous circuits designed using the
emerging CNTFET (Carbon NanoTube Field-Effect Transistor) technology. We showed
through simulation and modeling in HSPICE that NULL-Convention Logic devices
built on CNTFET technology can overcome current CMOS technology's massive
standby power usage and diminishing speed improvements.

--------------------------------------------------------------------------------

## Projects

### [Autonomous Maze Navigation Robot](https://gitlab.com/alexander-c-b/autonomous-maze-navigation-robot/)

![](https://gitlab.com/alexander-c-b/autonomous-maze-navigation-robot/-/raw/main/videos/overhead_perspective.webm)

The [Autonomous Maze Navigation
Robot](https://gitlab.com/alexander-c-b/autonomous-maze-navigation-robot/) was a
project by myself, [Jenelle
Fernando](https://www.linkedin.com/in/jenelle-fernando/) and [Omar
Mateo](https://www.linkedin.com/in/omar-mateo-056402225/) to create a robot
capable of navigating through an obstacle maze to the end without human
interaction or guidance. We utilized C++ and the Arduino Uno for autonomous
navigation.

![](https://gitlab.com/alexander-c-b/autonomous-maze-navigation-robot/-/raw/main/images/construction.png)

--------------------------------------------------------------------------------

### [Prototype Paper Calculator](https://gitlab.com/alexander-c-b/prototype-computer-vision-paper-calculator)

![](./videos/paper-calculator.webm)

This Python application utilizes computer vision techniques---specifically
[color
thresholding](https://www.ni.com/docs/en-US/bundle/ni-vision-concepts-help/page/color_segmentation.html)
and [background
subtraction](https://en.wikipedia.org/wiki/Foreground_detection)---to detect
when a person's finger touches the buttons of a calculator printed on paper. I
used the [OpenCv](https://opencv.org/) library to accomplish the thresholding,
background subtraction and blurring tasks, and also to display the video with
background subtraction applied (right) and without it (left).

--------------------------------------------------------------------------------

### [Project Portfolio](https://gitlab.com/alexander-c-b/portfolio)

The source code for this website. All content is written using the [Pandoc
Markdown](https://pandoc.org/MANUAL.html#philosophy) markup language, with basic
HTML and CSS files to structure and stylize the content, respectively. The
Makefile from [Bake](https://github.com/fcanas/bake) is used to automatically
regenerate the website and deploy it to GitLab Pages for hosting.

--------------------------------------------------------------------------------

### [Prototype Tilt-Controlled Mouse Pointer](https://gitlab.com/alexander-c-b/prototype-tilt-controlled-mouse-pointer)

![](./videos/microprocessors.webm)

For this class project in Microprocessors, I created a prototype tilt-controlled
desktop mouse cursor. First, my custom firmware for the AVR Atmega328p
microprocessor programmed in C gathered gyroscope orientation data. The
microprocessor then sent data via USART to a Python desktop application, which
finally used multithreading to simultaneously move the mouse cursor.

--------------------------------------------------------------------------------

### [Enhanced ASCII Math](https://gitlab.com/alexander-c-b/AsciiMath)

    x(t) = sin(2 π 30rad/s / 3 + θ).

This project started as a modification of
[Kerl13/AsciiMath](https://github.com/Kerl13/AsciiMath), with added features
including:

-   Writing measurement units (meters, feet, etc.) like `30m` or `150uF`
-   Aligning multiple equations based on the position of their equals signs.
-   Accepting Unicode symbols like `π` in place of just `pi` as in the original
    AsciiMath.

For instance, the equation above can be translated into LaTeX code like:

``` latex
x(t) = \sin \left( 2 \pi \frac{\si{30}{rad/s}}{3} + \theta \right).
```

The above code can then easily be rendered into a display format like the
following:

![](./images/latex-result.png)

--------------------------------------------------------------------------------

## [Feasibility Report: Improving Off-Campus Student Involvement](https://gitlab.com/alexander-c-b/commuter-engagement-feasibility-report)

![](./images/commuter.png)

This repository showcases the 20-page feasibility report and accompanying
presentation authored by myself, Matthew Knitter, Gavin Sweeney and Ethan
Montero on the subject of low commuter student engagement at Florida Polytechnic
University. We employed a survey of 127 Florida Polytechnic students to
understand our university's unique issues and potential solutions, and we
ultimately presented our findings and recommendation to the Department of
Student Affairs upon invitation. Ultimately, several recommendations were
incorporated into the new campus student lounge, which now provides a unique
space where off-campus students can rest and relax alongside residential
students. Some findings were also used in the development of Florida Polytechnic
University's new apartment shuttle program.

--------------------------------------------------------------------------------

## [NixOS Linux Desktop Environment](https://gitlab.com/alexander-c-b/desktop-environment)

Over more than two years, I gradually customized a desktop environment for NixOS
(and Arch Linux before that). My most important customizations involved a
command-line environment for editing and displaying documents using
[Markdown](https://daringfireball.net/projects/markdown/syntax#philosophy) for
drafting and [LaTeX](https://www.latex-project.org/) to typeset the final
product. I also incorporated the `dwm` "Dynamic Window Manager" to control
applications and windows almost entirely using the keyboard. A custom driver
script also enabled use of my [Wacom
Intuos](https://estore.wacom.com/en-US/wacom-intuos-s-black-us-ctl4100.html) pen
tablet as a more ergonomic mouse replacement.

--------------------------------------------------------------------------------

## [EAC Report](https://gitlab.com/alexander-c-b/eac-report)

A program written in Haskell to parse and summarize the log reports of Exact
Audio Copy (EAC). EAC can be used to digitize audio CDs for a modern listening
experience, but tracking errors in a large library of such digitzed CDs is
challenging. EAC's log format, which error information, is easily readable by
humans but fraught with difficult idiosyncrasies for computers to process. Thus,
EAC Report is a tool to parse the complex log format, extract important
information, and return results to the user in a document listing potential
library issues.
